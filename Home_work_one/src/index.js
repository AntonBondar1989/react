import React from "react";
import ReactDOM from "react-dom";

let getId = id => document.getElementById(id);

const Data = function(){
   return (
      <div>
         <TitleMonth></TitleMonth>
         <ul>
            <li>Січень</li>
            <li>Лютий</li>
            <li>Березень</li>
            <li>Квітень</li>
            <li>Травень</li>
            <li>Червень</li>
            <li>Липень</li>
            <li>Серпень</li>
            <li>Вересень</li>
            <li>Жовтень</li>
            <li>Листопад</li>
            <li>Грудень</li>
         </ul>

         <DayOfWeek></DayOfWeek>
         <ul>
            <li>Понеділок</li>
            <li>Вівторок</li>
            <li>Середа</li>
            <li>Четвер</li>
            <li>П'ятниця</li>
            <li>Субота</li>
            <li>Неділя</li>
         </ul>

         <Zodiac></Zodiac>
         <ul>
            <li>Овен</li>
            <li>Телець</li>
            <li>Близнюки</li>
            <li>Рак</li>
            <li>Лев</li>
            <li>Діва</li>
            <li>Терези</li>
            <li>Скорпіон</li>
            <li>Стрілець</li>
            <li>Козеріг</li>
            <li>Водолій</li>
            <li>Риби</li>
         </ul>
      </div>
   )
}

const TitleMonth = function(){
   return <h2>Назва місяців</h2>
}

const DayOfWeek = function(){
   return <h2>Дні тижня</h2>
}

const Zodiac = function(){
   return <h2>Зодіак</h2>
}


ReactDOM.render(<Data></Data>, getId("box"))